package com.example.catalogfilm.service.serviceImpl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
@RequiredArgsConstructor
public class TestService {

    private static final String URL_CB = "http://localhost:8080/test/circuitBreaker";

    private final RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private final CircuitBreakerFactory circuitBreakerFactory;

    public String getMessageCB() {

        log.info("Start to work getMessageCB");
        return circuitBreakerFactory.create("circuitMessage").run(
                () -> restTemplate.getForObject(URL_CB, String.class),
                throwable -> fallbackMessage()
        );
    }

    public String fallbackMessage() {
        log.info("Start to work fallbackMessage");
        return "Something goes wrong";
    }


}
