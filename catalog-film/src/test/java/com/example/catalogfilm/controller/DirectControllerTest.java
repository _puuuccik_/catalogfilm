package com.example.catalogfilm.controller;

import com.example.catalogfilm.model.Director;
import com.example.catalogfilm.service.serviceInterface.DirectorService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(DirectController.class)
class DirectControllerTest {

    @MockBean
    private DirectorService directorService;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private MockMvc mockMvc;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void shouldAddDirector() throws Exception {

        Director director = new Director();
        director.setUuid(UUID.randomUUID());
        director.setName("TestAdd");
        director.setAge(21);
        director.setCountry("USA");

        when(directorService.saveDirector(any(Director.class)))
                .thenReturn(director);

        mockMvc.perform(post("/director").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(director)))
                .andExpect(status().isOk()).andDo(print());
    }

    @Test
    void shouldGetDirector() throws Exception {

        var directorIdRand = UUID.randomUUID();
        Director director = new Director();
        director.setUuid(directorIdRand);
        director.setName("TestGet");
        director.setAge(21);
        director.setCountry("USA");

        Mockito.when(directorService.getDirector(Mockito.any()))
                .thenReturn(director);
        String requestUrl = "/director?directorId=" + directorIdRand;

        mockMvc.perform(get(requestUrl))
                .andExpect(status().isOk()).andDo(print())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.uuid").value(directorIdRand.toString()))
                .andExpect(jsonPath("$.name").value(director.getName()))
                .andExpect(jsonPath("$.age").value(director.getAge()))
                .andExpect(jsonPath("$.country").value(director.getCountry()));


    }
}