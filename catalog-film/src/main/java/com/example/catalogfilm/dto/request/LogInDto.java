package com.example.catalogfilm.dto.request;

import lombok.Data;

@Data
public class LogInDto {

    private String userName;
    private String password;
}
