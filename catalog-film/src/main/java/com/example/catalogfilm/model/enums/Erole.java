package com.example.catalogfilm.model.enums;

public enum Erole {

    ROLE_MODERATOR,
    ROLE_ADMIN,
    ROLE_USER
}
