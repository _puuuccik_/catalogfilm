package com.example.catalogfilm.controller;

import com.example.catalogfilm.config.securityConfig.JwtGenerator;
import com.example.catalogfilm.dto.request.LogInDto;
import com.example.catalogfilm.dto.request.RegistrationDto;
import com.example.catalogfilm.dto.response.JwtResponse;
import com.example.catalogfilm.model.Role;
import com.example.catalogfilm.model.User;
import com.example.catalogfilm.model.enums.Erole;
import com.example.catalogfilm.repository.RoleRepository;
import com.example.catalogfilm.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;
import java.util.Set;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/auth")
public class AuthController {

    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtGenerator jwtGenerator;

    @PostMapping("/signup")
    public ResponseEntity<?> registration(
            @RequestBody RegistrationDto registrationDto) {

        if (userRepository.existsByUserName(registrationDto.getUserName())) {
            return new ResponseEntity<>("Already exist", HttpStatus.BAD_REQUEST);
        }

        User createUser = new User();
        createUser.setUserName(registrationDto.getUserName());
        createUser.setPassword(passwordEncoder.encode(registrationDto.getPassword()));
        Set<Role> newRoles = new HashSet<>();
        Role userRole = roleRepository.findByRole(Erole.ROLE_USER)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
        newRoles.add(userRole);
        createUser.setRoles(newRoles);

        userRepository.save(createUser);
        return new ResponseEntity<>("User was added", HttpStatus.CREATED);
    }

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@RequestBody LogInDto logInDto) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(logInDto.getUserName(),
                        logInDto.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtGenerator.generateToken(authentication);

        return ResponseEntity.ok(new JwtResponse(jwt));
    }


}
