package com.example.catalogfilm.dto.request;

import lombok.Data;

@Data
public class RegistrationDto {

    private String userName;
    private String password;
}
