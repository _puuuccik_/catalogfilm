package com.example.catalogfilm.controller;


import com.example.catalogfilm.service.serviceImpl.Counter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Slf4j
public class CounterController {

    private final Counter counter;

    @GetMapping("/counter")
    public ResponseEntity<?> getCounter() {
        for (int i = 0; i < 3; i++) {
            counter.incrementMillion();
        }

        return ResponseEntity.ok("CounterComplete");
    }
}
