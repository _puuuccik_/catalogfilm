package com.example.catalogfilm.controller;

import com.example.catalogfilm.model.Film;
import com.example.catalogfilm.service.serviceInterface.FilmService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static com.example.catalogfilm.constants.GenreEnum.ROMANCE;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(FilmController.class)
class FilmControllerTest {

    @MockBean
    private FilmService filmService;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private MockMvc mockMvc;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void shouldAddDirector() throws Exception {

        Film film = new Film();
        film.setUuid(UUID.randomUUID());
        film.setGenre(ROMANCE);
        film.setRating(8);
        film.setTitle("Bomb");

        when(filmService.saveFilm(any(Film.class)))
                .thenReturn(film);

        mockMvc.perform(post("/film").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(film)))
                .andExpect(status().isOk()).andDo(print());
    }

    @Test
    void shouldGetDirector() throws Exception {

        Film film = new Film();
        film.setUuid(UUID.randomUUID());
        film.setGenre(ROMANCE);
        film.setRating(8);
        film.setTitle("Bomb2");

        Mockito.when(filmService.getFilm(Mockito.any()))
                .thenReturn(film);
        String requestUrl = "/film?filmId=" + film.getUuid();

        mockMvc.perform(get(requestUrl))
                .andExpect(status().isOk()).andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.uuid").value(film.getUuid().toString()))
                .andExpect(jsonPath("$.genre").value(film.getGenre().toString()))
                .andExpect(jsonPath("$.rating").value(film.getRating()))
                .andExpect(jsonPath("$.title").value(film.getTitle()));


    }
}