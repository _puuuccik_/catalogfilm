package com.example.catalogfilm.service.serviceImpl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@Service
public class Counter {

    private final AtomicInteger counter = new AtomicInteger(0);

    public int getValue() {
        return counter.get();
    }

    public void increment() {
        counter.incrementAndGet();
    }

    @Async
    public void incrementMillion() {
        for (int i = 0; i < 1000000; i++) {
            increment();
            log.info(String.valueOf(getValue()));
        }
    }
}
